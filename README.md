# EvolvedOrgansRedux - Move Fix

This RimWorld mod fixes EvolvedOrgansRedux so that pawns without legs cannot move.  This mod accomplishes the aforementioned feat by way of a simple XML patch.

## Installing

### Steam

1. Find this mod in the Steam Workshop.
1. Subscribe to the mod.
1. Enable the mod.
1. Sort your mods.

### Manually

1. Download the latest zip file from Releases.
1. Extract to your Mods/ folder.
1. Enable the mod.
1. Sort your mods.

### From Source

1. Install CPython >= 3.9.
1. Install poetry.
1. Clone the repository somewhere.
1. `cd /path/to/evolved-organs-redux-movefix`
1. `poetry shell`
1. `python devtools/genAboutXml.py`
1. `python devtools/build.py`
1. Copy the generated `dist/` folder contents to a directory in `Mods/`.

## Sorting

This mod contains the necessary About.xml file entries to auto-sort properly.

If you are unable to auto-sort for some reason, make sure it is placed *after* EvolvedOrgansRedux.

## License

MIT