from concurrent.futures import ThreadPoolExecutor
import os
from pathlib import Path
import shutil
from typing import Optional
from rich.progress import Progress
from lxml import etree
from humanfriendly import format_size as _format_size

SRC = Path("src")
DIST = Path("dist")


def format_size(l: int) -> str:
    o = _format_size(l)
    o = o.replace("bytes", "B")
    return o


def copyxml(prog: Progress, src: Path, dest: Optional[Path] = None) -> None:
    if dest is None:
        src = SRC / src
        dest = DIST / src
    oldsz = os.path.getsize(src)
    x: etree._ElementTree
    tid = prog.add_task("compacting", start=False, total=False, filename=src)
    dest.parent.mkdir(parents=True, exist_ok=True)
    prog.start_task(tid)
    with prog.open(src, "r", task_id=tid) as f:
        x = etree.parse(
            f,
            parser=etree.XMLParser(
                remove_blank_text=True, remove_comments=True, compact=True
            ),
        )
    prog.update(tid, total=None)
    with open(dest, "wb") as f:
        x.write(f, encoding="utf-8", xml_declaration=True)
    newsz = os.path.getsize(dest)
    f = (newsz / oldsz) * 100.0
    prog.remove_task(tid)
    prog.console.print(
        "[bold green]XML [/]",
        dest,
        f"(minified; {format_size(oldsz)}->{format_size(newsz)}; {f:0.2f}%)",
    )


def simplecopy(prog: Progress, src: Path, dest: Optional[Path] = None) -> None:
    if dest is None:
        src = SRC / src
        dest = DIST / src
    tid = prog.add_task("copying", start=False, total=False, filename=src)
    dest.parent.mkdir(parents=True, exist_ok=True)
    prog.start_task(tid)
    with prog.open(src, "rb", task_id=tid) as f:
        with dest.open("wb") as w:
            shutil.copyfileobj(f, w)
    prog.remove_task(tid)
    prog.console.print("[bold green]COPY[/]", dest)


def main() -> None:
    shutil.rmtree(DIST)
    with Progress() as prog:
        with ThreadPoolExecutor() as pool:
            simplecopy(
                prog,
                Path("README.md"),
            )
            copyxml(
                prog,
                Path("LoadFolders.xml"),
            )
            copyxml(
                prog,
                Path("1.4") / "News" / "UpdateFeatureDefs" / "UpdateFeatures.xml",
            )
            copyxml(
                prog,
                Path("1.4") / "Patches" / "AddConceptual.xml",
            )
            copyxml(
                prog,
                Path("About") / "About.xml",
            )
            simplecopy(
                prog,
                Path("About") / "meta.toml",
            )
            simplecopy(
                prog,
                Path("About") / "Preview.png",
            )
        prog.console.print("BUILD [bold green]COMPLETE![/]")


if __name__ == "__main__":
    main()
