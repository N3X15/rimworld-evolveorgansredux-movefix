import collections
from pathlib import Path
from typing import Dict, Final, List, Optional, Tuple, TypeVar, Union, Type, Any

import toml
from lxml import etree


T = TypeVar("T")
CF_TRUE: Final[str] = "true".casefold()

STRING_BECOMES_CDATA_IF_PRESENT: Final[str] = "\r\n\t<>&"


def write_xml_to(e: etree._Element, path: Path) -> None:
    tree: etree._ElementTree = etree.ElementTree(e)
    root: etree._Element = tree.getroot()
    for e in root.iter():
        t = e.text
        if t is not None:
            if any(c in t for c in STRING_BECOMES_CDATA_IF_PRESENT):
                t = etree.CDATA(t)
            e.text = t
    etree.indent(tree, space="\t")
    with open(path, "wb") as f:
        tree.write(f, encoding="utf-8")


class XMLEntity:
    TAG: str = ""

    def get_attrs(self) -> dict:
        return dict()

    def get_children(self) -> List[etree._Element | etree.CDATA]:
        return []

    def to_xml(self, tag: Optional[str] = None) -> etree._Element:
        e = etree.Element(tag or self.TAG, self.get_attrs())
        [e.append(c) for c in self.get_children()]
        return e

    def from_xml(self, e: etree._Element) -> None:
        assert e.tag == self.TAG
        pass

    def _get_str(
        self,
        e: etree._Element,
        elemName: str,
        default: Optional[str] = None,
        required: bool = False,
    ) -> Optional[str]:
        v = e.find(f"./{elemName}")
        if required and v is None:
            raise KeyError(elemName)
        return v.text if v is not None else default

    def _require_str(self, e: etree._Element, elemName: str) -> str:
        return self._get_str(e, elemName, default=None, required=True)

    def _get_int(
        self,
        e: etree._Element,
        elemName: str,
        default: Optional[int] = None,
        required: bool = False,
    ) -> Optional[int]:
        return int(self._get_str(e, elemName, required=required) or default)

    def _require_int(self, e: etree._Element, elemName: str) -> int:
        return int(self._get_str(e, elemName, default=None, required=True))

    def _get_float(
        self,
        e: etree._Element,
        elemName: str,
        default: Optional[float] = None,
        required: bool = False,
    ) -> Optional[float]:
        return float(self._get_str(e, elemName, required=required) or default)

    def _require_float(self, e: etree._Element, elemName: str) -> float:
        return float(self._get_str(e, elemName, default=None, required=True))

    def _load_if_tag_present(self, e: etree._Element, tag: str, cls: Type["XMLEntity"]):
        if (ve := e.find(tag)) is not None:
            v: XMLEntity = cls()
            v.from_xml(ve)
            return v
        else:
            return None

    def _append_str(self, e: etree._Element, elemName: str, value: str) -> None:
        etree.SubElement(
            e,
            elemName,
        ).text = value

    def _append_int(self, e: etree._Element, elemName: str, value: int) -> None:
        self._append_str(e=e, elemName=elemName, value=str(value))

    def _append_float(self, e: etree._Element, elemName: str, value: float) -> None:
        self._append_str(e=e, elemName=elemName, value=str(value))

    def _append_xml_if_not_none(
        self, e: etree._Element, elemName: str, value: "XMLEntity"
    ) -> None:
        if value is not None:
            e.append(value.to_xml(elemName))


class XMLBaseList(list[T], XMLEntity):
    def convert(self, e: Union[str, etree.CDATA, etree._Element]) -> T:
        raise NotImplementedError()

    def as_element_or_str(self, e: T | None) -> Union[etree._Element, etree.CDATA]:
        if isinstance(e, str):
            return etree.CDATA(e)
        elif isinstance(e, (int, float)):
            return etree.CDATA(str(e))
        elif isinstance(e, etree._Element):
            return e
        elif isinstance(e, XMLEntity):
            return e.to_xml()
        else:
            return None

    def get_children(self) -> List[etree._Element | etree.CDATA]:
        o = []
        for e in self:
            if (v := self.as_element_or_str(e)) is not None:
                li: etree._Element = etree.Element("li")
                if isinstance(v, str):
                    li.text = v
                else:
                    if isinstance(v, XMLEntity):
                        li.append(c.to_xml("li"))
                    else:
                        if isinstance(v, etree.CDATA):
                            li.text = v
                        else:
                            li.append(v)
                o.append(li)
        return o

    def from_xml(self, e: etree._Element) -> None:
        # super().from_xml(e)
        li: etree._Element
        for li in e:
            if li.tag == "li":
                self.read_li(li)

    def read_li(self, li: etree._Element) -> None:
        if len(li) > 0 or li.text is not None:
            self.append(self.convert(li.text if li.text is not None else li[0]))
        else:
            self.append(None)


class XMLStrList(XMLBaseList[str]):
    def convert(self, e: str | etree.CDATA | etree._Element) -> str:
        assert not isinstance(e, etree._Element)
        if isinstance(e, etree.CDATA):
            e = e.text
        return e


class XMLVersionList(XMLBaseList[Tuple[int, ...]]):
    def convert(self, e: str | etree.CDATA | etree._Element) -> Tuple[int, ...]:
        assert not isinstance(e, etree._Element)
        if isinstance(e, etree.CDATA):
            e = e.text
        return tuple([int(x) for x in e.strip().split(".")])

    def as_element_or_str(
        self, e: Tuple[int, ...] | None
    ) -> etree._Element | etree.CDATA:
        if e is None:
            return None
        return etree.CDATA(".".join(map(str, e)))


class XMLModVersion(XMLEntity):
    TAG = "modVersion"

    def __init__(
        self,
        version: Optional[Tuple[int, ...]] = None,
        ignoreIfNoMatchingField: bool = False,
    ) -> None:
        super().__init__()
        self.version: Tuple[int, ...] = version or (0, 0, 1)
        self.ignoreIfNoMatchingField: bool = ignoreIfNoMatchingField

    def get_attrs(
        self,
    ) -> dict:
        o: Dict[str, str] = {}
        if self.ignoreIfNoMatchingField:
            o["IgnoreIfNoMatchingField"] = "True"
        return o

    def to_xml(self, tag: Optional[str] = None) -> etree._Element:
        e: etree._Element = super().to_xml(tag=tag)
        e.text = ".".join(map(str, self.version))
        return e

    def from_xml(self, e: etree._Element) -> None:
        super().from_xml(e)
        self.version = tuple(map(int, str(e.text).strip().split(".")))
        self.ignoreIfNoMatchingField = (
            str(e.attrib.get("IgnoreIfNoMatchingField", "")).casefold() == CF_TRUE
        )


class XMLModDependency(XMLEntity):
    TAG = None

    def __init__(
        self,
        packageId: str = "",
        displayName: str = "",
        steamWorkshopUrl: str = "",
        downloadUrl: Optional[str] = None,
    ) -> None:
        super().__init__()
        self.packageId: str = packageId
        self.displayName: str = displayName
        self.steamWorkshopUrl: str = steamWorkshopUrl
        self.downloadUrl: Optional[str] = downloadUrl

    def from_xml(self, e: etree._Element) -> None:
        self.packageId = self._require_str(e, "packageId")
        self.displayName = self._require_str(e, "displayName")
        self.steamWorkshopUrl = self._require_str(e, "steamWorkshopUrl")
        self.downloadUrl = self._get_str(e, "downloadUrl")

    def to_xml(self, tag: Optional[str] = None) -> etree._Element:
        e: etree._Element = super().to_xml(tag=tag)
        etree.SubElement(e, "packageId").text = self.packageId
        etree.SubElement(e, "displayName").text = self.displayName
        etree.SubElement(e, "steamWorkshopUrl").text = self.steamWorkshopUrl
        if self.downloadUrl is not None:
            etree.SubElement(e, "downloadUrl").text = self.downloadUrl
        return e


class XMLModDependencyList(XMLBaseList[XMLModDependency]):
    def read_li(self, li: etree._Element) -> None:
        dep = XMLModDependency()
        dep.from_xml(li)
        self.append(dep)

    def get_children(self) -> List[etree._Element | etree.CDATA]:
        o = []
        for e in self:
            o.append(e.to_xml("li"))
        return o


class XMLBaseVersionDict(dict[Tuple[int, ...], T], XMLEntity):
    def _key2tag(self, k: Tuple[int, ...]) -> str:
        return "v" + (".".join(map(str, k)))

    def append_value_to(e: etree._Element, v: T) -> None:
        raise NotImplementedError()

    def get_children(self) -> List[etree._Element | etree.CDATA]:
        o: List[etree._Element | etree.CDATA] = []
        for k, v in self.items():
            e = etree.Element(self._key2tag(k))
            self.append_value_to(e, v)
            o.append(e)
        return o

    def extract_value_from(self, ke: etree._Element) -> T:
        raise NotImplementedError()

    def from_xml(self, e: etree._Element) -> None:
        ke: etree._Element
        for ke in e:
            k = tuple(map(int, ke.tag[1:].split(".")))
            v = self.extract_value_from(ke)
            self[k] = v


class XMLVersionedModDependencies(XMLBaseVersionDict[XMLModDependencyList]):
    def append_value_to(e: etree._Element, v: XMLModDependencyList) -> None:
        for dep in v:
            e.append(dep.to_xml())

    def extract_value_from(self, ke: etree._Element) -> XMLModDependencyList:
        ve: etree._Element
        v = XMLModDependencyList()
        v.from_xml(ke)
        return v


class XMLVersionedStrList(XMLBaseVersionDict[XMLStrList]):
    def append_value_to(e: etree._Element, v: XMLStrList) -> None:
        for ve in v.get_children():
            e.append(ve)

    def extract_value_from(self, ke: etree._Element) -> XMLStrList:
        v = XMLStrList()
        v.from_xml(ke)
        return v


class AboutXML(XMLEntity):
    TAG = "ModMetaData"

    def __init__(self) -> None:
        self.packageId: str = ""
        self.name: str = ""
        self.authors: XMLStrList[str] = XMLStrList()
        self.description: str = ""
        self.supportedVersions: XMLVersionList = XMLVersionList([(1, 4)])

        self.modVersion: Optional[XMLModVersion] = None
        self.url: Optional[str] = None
        self.descriptionsByVersion: Optional[XMLVersionedStrList] = None
        self.modDependencies: Optional[XMLModDependencyList] = None
        self.modDependenciesByVersion: Optional[XMLVersionedModDependencies] = None
        self.loadBefore: Optional[XMLStrList] = None
        self.loadBeforeByVersion: Optional[XMLVersionedStrList] = None
        self.forceLoadBefore: Optional[XMLStrList] = None
        self.loadAfter: Optional[XMLStrList] = None
        self.loadAfterByVersion: Optional[XMLVersionedStrList] = None
        self.forceLoadAfter: Optional[XMLStrList] = None
        self.incompatibleWith: Optional[XMLStrList] = None
        self.incompatibleWithByVersion: Optional[XMLVersionedStrList] = None

    def from_xml(self, e: etree._Element) -> None:
        super().from_xml(e)
        ae: etree._Element
        self.packageId = self._require_str(e, "packageId")
        self.name = self._require_str(e, "name")
        if (ae := e.find("author")) is not None:
            self.authors = XMLStrList([ae.text])
        else:
            self.authors.from_xml(e.find("authors"))
        self.description = self._require_str(e, "description")
        self.supportedVersions.from_xml(e.find("supportedVersions"))

        self.modVersion = self._load_if_tag_present(e, "modVersion", XMLModVersion)
        self.url = self._get_str(e, "url")
        self.descriptionsByVersion = self._load_if_tag_present(
            e, "descriptionsByVersion", XMLVersionedStrList
        )
        self.modDependencies = self._load_if_tag_present(
            e, "modDependencies", XMLModDependencyList
        )
        self.modDependenciesByVersion = self._load_if_tag_present(
            e, "modDependenciesByVersion", XMLVersionedModDependencies
        )

        self.loadBefore = self._load_if_tag_present(e, "loadBefore", XMLStrList)
        self.loadBeforeByVersion = self._load_if_tag_present(
            e, "loadBeforeByVersion", XMLVersionedStrList
        )
        self.forceLoadBefore = self._load_if_tag_present(
            e, "forceLoadBefore", XMLStrList
        )

        self.loadAfter = self._load_if_tag_present(e, "loadAfter", XMLStrList)
        self.loadAfterByVersion = self._load_if_tag_present(
            e, "loadAfterByVersion", XMLVersionedStrList
        )
        self.forceLoadAfter = self._load_if_tag_present(e, "forceLoadAfter", XMLStrList)

        self.incompatibleWith = self._load_if_tag_present(
            e, "incompatibleWith", XMLStrList
        )
        self.incompatibleWithByVersion = self._load_if_tag_present(
            e, "incompatibleWithByVersion", XMLVersionedStrList
        )

    def to_xml(self, tag: Optional[str] = None) -> etree._Element:
        e = super().to_xml(tag=tag)
        self._append_str(e, "packageId", self.packageId)
        self._append_str(e, "name", self.name)
        e.append(self.authors.to_xml("authors"))
        self._append_str(e, "description", self.description)
        e.append(self.supportedVersions.to_xml("supportedVersions"))

        self._append_xml_if_not_none(e, "modVersion", self.modVersion)
        if self.url is not None:
            self._append_str(e, "url", self.url)
        self._append_xml_if_not_none(
            e, "descriptionsByVersion", self.descriptionsByVersion
        )
        self._append_xml_if_not_none(e, "modDependencies", self.modDependencies)
        self._append_xml_if_not_none(
            e, "modDependenciesByVersion", self.modDependenciesByVersion
        )
        self._append_xml_if_not_none(e, "loadBefore", self.loadBefore)
        self._append_xml_if_not_none(e, "loadBeforeByVersion", self.loadBeforeByVersion)
        self._append_xml_if_not_none(e, "forceLoadBefore", self.forceLoadBefore)
        self._append_xml_if_not_none(e, "loadAfter", self.loadAfter)
        self._append_xml_if_not_none(e, "loadAfterByVersion", self.loadAfterByVersion)
        self._append_xml_if_not_none(e, "forceLoadAfter", self.forceLoadAfter)
        self._append_xml_if_not_none(e, "incompatibleWith", self.incompatibleWith)
        self._append_xml_if_not_none(
            e, "incompatibleWithByVersion", self.incompatibleWithByVersion
        )
        return e


def mkAboutXML(tomlpath: Path, path: Path) -> None:
    data: Dict[str, Any]
    with open(tomlpath, "r") as f:
        data = toml.load(f)
    package: Dict[str, Any] = data["package"]
    about = AboutXML()
    about.packageId = package["id"]
    about.name = package["name"]
    if "url" in package:
        about.url = package["url"]
    about.modVersion = XMLModVersion()
    about.modVersion.version = tuple(map(int, package["version"].split(".")))
    about.description = package["description"]
    about.supportedVersions = XMLVersionList([(1, 4)])
    about.authors = XMLStrList(package["authors"])
    if "dependencies" in package:
        about.modDependencies = XMLModDependencyList()
        for depdata in package["dependencies"]:
            moddep = XMLModDependency(
                packageId=depdata["id"],
                displayName=depdata["name"],
                steamWorkshopUrl=depdata["workshop-url"],
                downloadUrl=depdata.get("download-url"),
            )
            about.modDependencies.append(moddep)
    if "load-after" in package:
        about.loadAfter = XMLStrList(package.get("load-after"))
    if "load-before" in package:
        about.loadBefore = XMLStrList(package.get("load-before"))

    write_xml_to(about.to_xml(), path)

    # data['package']['dependencies']=[{
    #     'id':'EvolvedOrgansRedux',
    #     'name':'EvolvedOrgansRedux',
    #     'workshop-url':'https://steamcommunity.com/sharedfiles/filedetails/?id=2424381146',
    # }]

    # with open(ABOUT/'meta.new.toml','w') as f:
    #     toml.dump(data, f)


SRC = Path("src")
ABOUT = SRC / "About"
DEVTOOLS = Path("devtools")
# dumpAboutXML(ABOUT / 'About.xml', DEVTOOLS / 'meta.test.toml')
mkAboutXML(ABOUT / "meta.toml", ABOUT / "About.xml")
