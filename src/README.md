# EvolvedOrgansRedux - Move Fix

This mod fixes an issue with EvolvedOrgansRedux permitting pawns to move when they have no legs.

You're looking at the compiled (minified and compressed) version of the mod.

For a more human-readable experience, please visit <https://gitlab.com/N3X15/evolvedorgansredux-movefix>.